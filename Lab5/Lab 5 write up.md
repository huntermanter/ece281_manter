# Lab 5

### Hunter Manter

**Documentation statement:** None

**Part 1 and 2:** both should be found in the lab worksheet.  The answers to the questions are in the worksheet.

**Reflection**

- Number of hours spent: 5
- The portion that was most difficult was trying to keep track of what all the signals did.
- I found lesson 34 the most helpful on this lab.  It allowed me to go back and check myself on the basics.
- I think the lab was good and easy to figure out.

