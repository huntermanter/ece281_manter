![Utilization summary](Utilization summary.PNG)



1. The number of components in the report agrees with my design because with all the mux's and the adder, as well as the miscellaneous ands and ors should add up.
2. My input and output buffers makes sense because there were 2 inputs with 32 bits, 1 output with 32 bits, 1 output with 1 bit, and one input with 3 bits.  That all adds up to 100.  This is expected when adding 2 32 bit numbers and using mux's.





Feedback

1. Number of hours spent working on this was 5-6 hours
2. Suggestions to improve this would probably be a recap of signals.  I was slightly confused with the inputs, outputs, and intermediate signals that I had to worry about.  Otherwise it wasn't too bad.