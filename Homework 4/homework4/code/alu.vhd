--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : ECE_template.vhd
--| AUTHOR(S)     : C9C John Doe, C8C Jane Day
--| CREATED       : 01/01/2017
--| DESCRIPTION   : This file simply provides a template for all VHDL assignments
--| 				- Be sure to include your Documentation Statement below!
--|
--| DOCUMENTATION : 
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : LIST ANY DEPENDENCIES
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  use ieee.std_logic_signed.all;

library unisim;
  use UNISIM.Vcomponents.ALL;

-- entity name should match filename  
entity alu is
       port(
        a, b : in std_logic_vector(31 downto 0);
        f : in std_logic_vector(2 downto 0);
        y : out std_logic_vector(31 downto 0);
        zero : out std_logic
    );
end alu;

architecture alu_arch of alu is 
	-- include components declarations and signals
	signal i_A, i_B, c_MUXTOBEORNOTTOBE, c_ZEROEX, c_ADD, c_OR,  c_AND, o_MUXOUT : std_logic_vector(31 downto 0) := x"00000000";
	signal c_MSB : std_logic := '0';
	-- intermediate signals with initial value
	-- typically you would use names that relate to signal (e.g. c_mux_2)
  
begin
	-- PORT MAPS ----------------------------------------
		i_A <= a;
		i_B <= b;
		c_MUXTOBEORNOTTOBE <= i_B when (f(2) = '0') else not b;
		c_OR <= c_MUXTOBEORNOTTOBE or i_A;
		c_AND <= c_MUXTOBEORNOTTOBE and i_A;
		c_ADD <= (i_A + c_MUXTOBEORNOTTOBE +f(2));
		c_MSB <=c_ADD(31);
		c_ZEROEX <= x"00000001" when (c_MSB = '1') else x"00000000";
		o_MUXOUT <= c_AND when (f(0)  = '0' and f(1) = '0')
					else c_OR when (f(0) = '1' and f(1) = '0')
					else c_ADD when (f(0) = '0' and f(1) = '1')
					else c_ZEROEX;
		y <= o_MUXOUT;
		zero <= '1' when o_MUXOUT = x"00000000"  else '0';
	-- map ports for any component instantiations (port mapping is like wiring hardware)
	

	-- CONCURRENT STATEMENTS "MODULES" ------------------

	-- Provide a comment that describes each "module" as appropriate
	-- think of "modules" in this sense as groups of related statements
		

		-- PROCESSES ----------------------------------------
	
	-- Provide a comment that describes each process
	-- block them off like the modules above and separate with SPACE
	-- Note, the example below is a local oscillator address counter 
	--	not related to other code in this file
	
	
	
end alu_arch;
