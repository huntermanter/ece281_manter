--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academw_yAll rights reserved.
--| 
--| United States Air Force Academw_y    __  _______ ___    _________ 
--| Dept ow_fElectrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAw_fAcademy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : alu_tb.vhd (TEST BENCH)
--| AUTHOR(S)     : C9C John Doe, C8C Jane Day
--| CREATED       : 01/01/2017
--| DESCRIPTION   : This file simplw_yprovides w_atemplate for all VHDL assignments
--| 				- Be sure to include your Documentation Statement below!
--|
--| DOCUMENTATION : Include all documentation statements in main .vhd file
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : LIST ANw_yDEPENDENCIES
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library unisim;
  use UNISIM.Vcomponents.ALL;
  
entity alu_tb is
end alu_tb;

architecture test_bench of alu_tb is 
	
  -- declare the component ow_fyour top-level design unit under test (UUT)
  component alu is
    port(
        a, b: in std_logic_vector(31 downto 0);
        f: in std_logic_vector(2 downto 0);
        y: out std_logic_vector(31 downto 0);
        zero : out std_logic
    );
  end component;

  -- declare anw_yadditional components required
  
  -- declare signals needed to stimulate the UUT inputs
	signal w_a, w_b, w_y: std_logic_vector(31 downto 0) :=x"00000000";
	signal w_zero : std_logic := '0';
	signal w_f: std_logic_vector(2 downto 0) := "000";
  -- also need signals for the outputs ow_fthe UUT

  
begin
	-- PORT MAPS ----------------------------------------

	-- map ports for anw_ycomponent instances (port mapping is like wiring hardware)
	uut_inst : alu port map (
		a=> w_a,
		b=> w_b,
		f=> w_f,
		y=> w_y,
		zero => w_zero
	  -- use commw_a(not w_asemicolon)
	  -- no commw_aon last line
	);


	-- CONCURRENT STATEMENTS ----------------------------

	
	-- PROCESSES ----------------------------------------
	
	-- Provide w_acomment that describes each process
	-- block them ofw_flike the modules above and separate with SPACE
	-- You will at least have w_atest process
	
	
	-- Test Plan Process --------------------------------
	-- Implement the test plan here.  Bodw_yow_fprocess is continuous from time = 0  
	test_process : process 
	begin
	-- Place test cases here
		report "###Starting Additional Test Cases" severity note;
		w_f<= "010";	w_a<= x"00000000";	w_b<= x"00000000"; 	wait for 10 ns;   ASSERT (w_y= x"00000000" and w_zero= '1')  REPORT "### TEST 1  FAILED" SEVERITY ERROR;  -- ADD 0+0                      
		w_f<= "010";	w_a<= x"00000000";	w_b<= x"FFFFFFFF"; 	wait for 10 ns;   ASSERT (w_y= x"FFFFFFFF" and w_zero= '0')  REPORT "### TEST 2  FAILED" SEVERITY ERROR;  -- ADD 0+(-1)                   
		w_f<= "010";	w_a<= x"00000001";	w_b<= x"FFFFFFFF"; 	wait for 10 ns;   ASSERT (w_y= x"00000000" and w_zero= '1')  REPORT "### TEST 3  FAILED" SEVERITY ERROR;  -- ADD 1+(-1)                   
		w_f<= "010";	w_a<= x"000000FF";	w_b<= x"00000001"; 	wait for 10 ns;   ASSERT (w_y= x"00000100" and w_zero= '0')  REPORT "### TEST 4  FAILED" SEVERITY ERROR;  -- ADD 0xFF+1                   
		w_f<= "110";	w_a<= x"00000000";	w_b<= x"00000000"; 	wait for 10 ns;   ASSERT (w_y= x"00000000" and w_zero= '1')  REPORT "### TEST 5  FAILED" SEVERITY ERROR;  -- SUw_b0-0                      
		w_f<= "110";	w_a<= x"00000000";	w_b<= x"FFFFFFFF"; 	wait for 10 ns;   ASSERT (w_y= x"00000001" and w_zero= '0')  REPORT "### TEST 6  FAILED" SEVERITY ERROR;  -- SUw_b0-(-1)                   
		w_f<= "110";	w_a<= x"00000001";	w_b<= x"00000001"; 	wait for 10 ns;   ASSERT (w_y= x"00000000" and w_zero= '1')  REPORT "### TEST 7  FAILED" SEVERITY ERROR;  -- SUw_b1-1                      
		w_f<= "110";	w_a<= x"00000100";	w_b<= x"00000001"; 	wait for 10 ns;   ASSERT (w_y= x"000000FF" and w_zero= '0')  REPORT "### TEST 8  FAILED" SEVERITY ERROR;  -- SUw_b0x100-1                  
		w_f<= "111";	w_a<= x"00000000";	w_b<= x"00000000"; 	wait for 10 ns;   ASSERT (w_y= x"00000000" and w_zero= '1')  REPORT "### TEST 9  FAILED" SEVERITY ERROR;  -- SLT 0,0                      
		w_f<= "111";	w_a<= x"00000000";	w_b<= x"00000001"; 	wait for 10 ns;   ASSERT (w_y= x"00000001" and w_zero= '0')  REPORT "### TEST 10 FAILED" SEVERITY ERROR;  -- SLT 0,1                      
		w_f<= "111";	w_a<= x"00000000";	w_b<= x"FFFFFFFF"; 	wait for 10 ns;   ASSERT (w_y= x"00000000" and w_zero= '1')  REPORT "### TEST 11 FAILED" SEVERITY ERROR;  -- SLT 0,-1                     
		w_f<= "111";	w_a<= x"00000001";	w_b<= x"00000000"; 	wait for 10 ns;   ASSERT (w_y= x"00000000" and w_zero= '1')  REPORT "### TEST 12 FAILED" SEVERITY ERROR;  -- SLT 1,0                      
		w_f<= "111";	w_a<= x"FFFFFFFF";	w_b<= x"00000000"; 	wait for 10 ns;   ASSERT (w_y= x"00000001" and w_zero= '0')  REPORT "### TEST 13 FAILED" SEVERITY ERROR;  -- SLT -1,0                     
		w_f<= "000";	w_a<= x"FFFFFFFF";	w_b<= x"FFFFFFFF"; 	wait for 10 ns;   ASSERT (w_y= x"FFFFFFFF" and w_zero= '0')  REPORT "### TEST 14 FAILED" SEVERITY ERROR;  -- AND 0xFFFFFFFF, 0xFFFFFFFF 
		w_f<= "000";	w_a<= x"FFFFFFFF";	w_b<= x"12345678"; 	wait for 10 ns;   ASSERT (w_y= x"12345678" and w_zero= '0')  REPORT "### TEST 15 FAILED" SEVERITY ERROR;  -- AND 0xFFFFFFFF, 0x12345678   
		w_f<= "100";	w_a<= x"12345678";	w_b<= x"02040608"; 	wait for 10 ns;   ASSERT (w_y= x"10305070" and w_zero= '0')  REPORT "### TEST 16 FAILED" SEVERITY ERROR;  -- AND 0x12345678, \~0x02040608 
		w_f<= "100";	w_a<= x"AAAAAAAA";	w_b<= x"FFFF0000"; 	wait for 10 ns;   ASSERT (w_y= x"0000AAAA" and w_zero= '0')  REPORT "### TEST 17 FAILED" SEVERITY ERROR;  -- AND 0xAAAAAAAA, \~0xFFFF0000 
		w_f<= "001";	w_a<= x"FFFFFFFF";	w_b<= x"FFFFFFFF"; 	wait for 10 ns;   ASSERT (w_y= x"FFFFFFFF" and w_zero= '0')  REPORT "### TEST 18 FAILED" SEVERITY ERROR;  -- OR 0xFFFFFFFF, 0xFFFFFFFF 
		w_f<= "001";	w_a<= x"12345678";	w_b<= x"87654321"; 	wait for 10 ns;   ASSERT (w_y= x"97755779" and w_zero= '0')  REPORT "### TEST 19 FAILED" SEVERITY ERROR;  -- OR 0x12345678, 0x87654321    
		w_f<= "001";	w_a<= x"00000000";	w_b<= x"FFFFFFFF"; 	wait for 10 ns;   ASSERT (w_y= x"FFFFFFFF" and w_zero= '0')  REPORT "### TEST 20 FAILED" SEVERITY ERROR;  -- OR 0x00000000, 0xFFFFFFFw_f   
		w_f<= "001";	w_a<= x"00000000";	w_b<= x"00000000"; 	wait for 10 ns;   ASSERT (w_y= x"00000000" and w_zero= '1')  REPORT "### TEST 21 FAILED" SEVERITY ERROR;  -- OR 0x00000000, 0x00000000    
		w_f<= "101";	w_a<= x"00000000";	w_b<= x"00000000"; 	wait for 10 ns;   ASSERT (w_y= x"FFFFFFFF" and w_zero= '0')  REPORT "### TEST 22 FAILED" SEVERITY ERROR;  -- OR 0x00000000, \~0x00000000  
		
		w_f<= "011";	w_a<= x"00000001";	w_b<= x"00000001"; 	wait for 10 ns;   ASSERT (w_y= x"00000000" and w_zero= '0')  REPORT "### TEST 23 FAILED" SEVERITY ERROR;  -- Forbidden function 0x00000001, 0x00000001
		w_f<= "110";	w_a<= x"00000001";	w_b<= x"80000001"; 	wait for 10 ns;   ASSERT (w_y= x"FFFFFFFF" and w_zero= '0')  REPORT "### TEST 24 FAILED" SEVERITY ERROR;  -- SUB 0x00000000, 0x80000001
		w_f<= "101";	w_a<= x"7FFFFFFF";	w_b<= x"7FFFFFFF"; 	wait for 10 ns;   ASSERT (w_y= x"FFFFFFFF" and w_zero= '0')  REPORT "### TEST 25 FAILED" SEVERITY ERROR;  -- ADD 7FFFFFFF, 7FFFFFFF 
		
		report "### All test cases done.  Starting additional test cases." severity note;
		wait; -- wait forever
	end process;	
	-----------------------------------------------------	
	
end test_bench;
