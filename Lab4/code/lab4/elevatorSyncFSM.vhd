--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : elevatorSyncFSM.vhd
--| AUTHOR(S)     : C9C John Doe, C8C Jane Day
--| CREATED       : 01/01/2017
--| DESCRIPTION   : This file simply provides a template for all VHDL assignments
--| 				- Be sure to include your Documentation Statement below!
--|
--| DOCUMENTATION : 
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : LIST ANY DEPENDENCIES
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  use ieee.std_logic_signed.all;

  
library unisim;
  use UNISIM.Vcomponents.ALL;
-- entity name should match filename  
entity elevatorSyncFSM is 
  port(
	-- Identify input and output bits here
	i_elevator1 : in std_logic_vector(3 downto 0);
	i_passengerFloor : in std_logic_vector(3 downto 0);
	i_desiredFloor : in std_logic_vector(3 downto 0);
	i_reset   : in  STD_LOGIC;
	i_go : in STD_LOGIC;
	i_clk : in std_logic;
	o_elevatorStop : out std_logic;
	o_up : out std_logic;
	o_down : out std_logic
  );
end elevatorSyncFSM;

architecture elevatorSyncFSM_arch of elevatorSyncFSM is 
	-- include components declarations and signals
	-- intermediate signals with initial value
	-- typically you would use names that relate to signal (e.g. c_mux_2)
	signal elevatorState : std_logic_vector(3 downto 0) := "0000";
	signal elevatorStateNext : std_logic_vector(3 downto 0) := "0000";
	signal passenger : std_logic_vector(3 downto 0);
	signal desired : std_logic_vector (3 downto 0);
	signal up : std_logic := '0';
	signal down : std_logic := '0';
begin
		
	
	
	-- PORT MAPS ----------------------------------------
	o_up <= up;
	o_down <= down;
	--will be stopped.  This should be when the elevator is is waiting for a command(00)
	--or pausing at the floor(10)
	o_elevatorStop <= '1' when (elevatorState = "0001" or elevatorState = "0100")
 else
								'0';
	--When the elevator should be going up.  This is when the elevator is below where it should be and when it's been sent
	up <='1' when  ((i_elevator1 < i_passengerFloor) and elevatorState = "0010") else
		 	'1' when ((i_elevator1 < i_desiredFloor) and elevatorState = "1000") else
				'0' when ((i_elevator1> i_passengerFloor) and elevatorState = "0010") else
				'0' when ((i_elevator1> i_desiredFloor) and elevatorState = "1000") 
				else '0';
	--when elevator is above passenger and should go to the passenger or when elevator is above the final floor after picking
	--up the passenger
	down <='0' when ((i_elevator1 < i_passengerFloor)  and elevatorState = "0010") else
				'0' when ((i_elevator1 < i_desiredFloor)  and elevatorState = "1000") else
				'1' when ((i_elevator1 > i_passengerFloor)  and elevatorState = "0010") else
				'1' when ((i_elevator1 > i_desiredFloor)  and elevatorState = "1000") else
				'0';
	--determines which state the elevator should be in.  01 to go to passenger. 10 to pause. 11 to go to second floor. 00 to be stopped.				
	elevatorStateNext <= "0010" when (not (i_elevator1 = passenger) and elevatorState = "0010") else
									"0100" when ((i_elevator1 = passenger) and elevatorState = "0010") else
									"1000" when (elevatorState = "0100") else
									"1000" when (not (i_elevator1 = desired) and elevatorState = "1000") else
									"0001" when ((i_elevator1 = desired) and elevatorState = "1000")
									else "0001";

	register_proc : process (i_go, i_clk, i_reset)
	begin
		if(i_reset = '1') then
			elevatorState <= "0001";
			passenger <= "0000";
			desired <= "0000";
		elsif(i_go='1') then
			passenger <= i_passengerFloor;
			desired <= i_desiredFloor;
			elevatorState <= "0010";
		elsif(rising_edge(i_clk)) then
			elevatorState <= elevatorStateNext;
		end if;			
	end process register_proc;
end elevatorSyncFSM_arch;
