--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : thunderbird_fsm.vhd
--| AUTHOR(S)     : C9C John Doe, C8C Jane Day
--| CREATED       : 01/01/2017
--| DESCRIPTION   : This file simply provides a template for all VHDL assignments
--| 				- Be sure to include your Documentation Statement below!
--|
--| DOCUMENTATION : 
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : LIST ANY DEPENDENCIES
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library unisim;
  use UNISIM.Vcomponents.ALL;

-- entity name should match filename  
entity thunderbird_fsm is
	port(
		i_clk, i_reset : in std_logic;
		i_left, i_right : in std_logic;
		o_lights_L : out std_logic_vector(2 downto 0);
		o_lights_R : out std_logic_vector(2 downto 0)
);
end thunderbird_fsm;

architecture thunderbird_fsm_arch of thunderbird_fsm is 
	-- include components declarations and signals
	signal s_cS1, s_cS2, s_cS3: std_logic := '0';
	signal s_nS1, s_nS2, s_nS3: std_logic := '0';
	signal c_LA, c_LB, c_LC, c_RA, c_RB, c_RC: std_logic := '0';
	
	-- intermediate signals with initial value
	-- typically you would use names that relate to signal (e.g. c_mux_2)
  
begin
	-- PORT MAPS ----------------------------------------
	o_lights_L(2) <=c_LA;
	o_lights_L(1) <=c_LB;
	o_lights_L(0) <=c_LC;
	o_lights_R(0) <=c_RA;
	o_lights_R(1) <=c_RB;
	o_lights_R(2) <=c_RC;
	
	
	s_nS1 <= (i_right and not s_cS1 and not s_cS2 and not s_cS3) or (s_cS1 and not s_cS2);
    s_nS2 <= (i_left and i_right and not s_cS1 and not s_cS2 and not s_cS3) or (not s_cS1 and s_cS2 and not s_cS3) or (not s_cS1 and not s_cS2 and s_cS3) or (s_cS1 and not s_cS2 and s_cS3);
    s_nS3 <= (i_left and i_right and not s_cS1 and not s_cS2 and not s_cS3) or (s_cS1 and not s_cS2 and not s_cS3) or (i_left and not s_cS1 and not s_cS2 and not s_cS3) or (s_cS2 and not s_cS1 and not s_cS3);
	
	
	flip_S1 : process(i_clk, i_reset)
	    begin
		if(i_reset = '1') then
			s_cS1 <= '0';
			s_cS2 <= '0';
			s_cS3 <= '0';
		elsif (rising_edge(i_clk)) then
			s_cS1 <= s_nS1;
			s_cS2 <= s_nS2;
			s_cS3 <= s_nS3;
		end if;
	end process;
	
	c_LA <= (not s_cS1 and (s_cS2 or s_cS3)) or (s_cS1 and s_cS2 and s_cS3);
	c_LB <= (not s_cS1 and s_cS2) or (s_cS2 and s_cS3);
	c_LC <= s_cS2 and s_cS3;
	c_RA <= s_cS1;
	c_RB <= s_cS1 and (s_cS2 or s_cS3);
	c_RC <= s_cS1 and s_cS2;
	
	-- map ports for any component instantiations (port mapping is like wiring hardware)
	

	-- CONCURRENT STATEMENTS "MODULES" ------------------

	-- Provide a comment that describes each "module" as appropriate
	-- think of "modules" in this sense as groups of related statements
		

		-- PROCESSES ----------------------------------------
	
	-- Provide a comment that describes each process
	-- block them off like the modules above and separate with SPACE
	-- Note, the example below is a local oscillator address counter 
	--	not related to other code in this file
	
	
	
end thunderbird_fsm_arch;
