--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : elevatorDecoder.vhd
--| AUTHOR(S)     : C9C John Doe, C8C Jane Day
--| CREATED       : 01/01/2017
--| DESCRIPTION   : This file simply provides a template for all VHDL assignments
--| 				- Be sure to include your Documentation Statement below!
--|
--| DOCUMENTATION : 
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : LIST ANY DEPENDENCIES
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

-- entity name should match filename  
entity elevatorDecoder is 
  port(
	i_elevator : in std_logic;
	i_up : in std_logic;
	i_down : in std_logic;
	i_stop : in std_logic;
	o_up1 :  out std_logic;
	o_down1 : out std_logic;
	o_stop1 : out std_logic;
	o_up2 : out std_logic;
	o_down2 : out std_logic;
	o_stop2 : out std_logic
  );
end elevatorDecoder;

architecture elevatorDecoder_arch of elevatorDecoder is 
begin
	
	o_up1 <= i_up when i_elevator = '0' else
					'0';
	o_down1 <= i_down when i_elevator = '0' else
					'0';
	o_stop1 <= i_stop when i_elevator = '0' else
					'1';
	o_up2 <= i_up when i_elevator = '1' else
					'0';
	o_down2 <= i_down when i_elevator = '1' else
					'0';
	o_stop2 <= i_stop when i_elevator = '1' else
					'1';
	
end elevatorDecoder_arch;
