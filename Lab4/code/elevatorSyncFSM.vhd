--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : elevatorSyncFSM.vhd
--| AUTHOR(S)     : C9C John Doe, C8C Jane Day
--| CREATED       : 01/01/2017
--| DESCRIPTION   : This file simply provides a template for all VHDL assignments
--| 				- Be sure to include your Documentation Statement below!
--|
--| DOCUMENTATION : 
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : LIST ANY DEPENDENCIES
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  use ieee.std_logic_signed.all;

  
library unisim;
  use UNISIM.Vcomponents.ALL;
-- entity name should match filename  
entity elevatorSyncFSM is 
  port(
	-- Identify input and output bits here
	i_elevator1 : in std_logic_vector(3 downto 0);
	i_elevator2 : in std_logic_vector(3 downto 0);
	i_passengerFloor : in std_logic_vector(3 downto 0);
	i_desiredFloor : in std_logic_vector(3 downto 0);
	i_reset   : in  STD_LOGIC;
	i_go : in STD_LOGIC;
	i_clk : in std_logic;
	o_elevatorChoice : out std_logic;
	o_elevatorStop : out std_logic;
	o_up : out std_logic;
	o_down : out std_logic
  );
end elevatorSyncFSM;

architecture elevatorSyncFSM_arch of elevatorSyncFSM is 
	-- include components declarations and signals
	-- intermediate signals with initial value
	-- typically you would use names that relate to signal (e.g. c_mux_2)
	signal elevatorState : unsigned := "00";
	signal elevatorStateNext : unsigned := "00";
	signal passenger : std_logic_vector(3 downto 0)	:= "0000";
	signal desired : std_logic_vector (3 downto 0) := "0000";
	signal elevatorChoice : std_logic;
	signal elevatorChoiceNext : std_logic;
	signal up : std_logic := '0';
	signal down : std_logic := '0';
	signal elevator : unsigned := "0000";
begin
		
	
	
	-- PORT MAPS ----------------------------------------
	o_up <= up;
	o_down <= down;
	o_elevatorChoice <= elevatorChoice;
	
	elevatorChoiceNext <= '1' when ((i_elevator1 - i_passengerFloor) > (i_elevator2 -i_passengerFloor) and elevatorStateNext  = "00") else
								'0' when ((i_elevator1 - i_passengerFloor) < (i_elevator2 -i_passengerFloor) and elevatorStateNext  = "00") else
								 '0';
	
	elevator <= i_elevator1 when elevatorChoice = '0' else
					i_elevator2 when elevatorChoice = '1';
	--Decide when the elevator will be stopped.  This should be when the elevator is is waiting for a command(00)
	--or pausing at the floor(10)
	o_elevatorStop <= '1' when elevatorState = "00" or elevatorStateNext = "10" else
								'0';
	--When the elevator should be going up.  This is when the elevator is below where it should be and when it's been sent
	up <='1' when  elevator< passenger and elevatorStateNext = "01" else
		 	'1' when elevator< desired and elevatorStateNext = "11" else
				'0';
	--when elevator is above passenger and should go to the passenger or when elevator is above the final floor after picking
	--up the passenger
	down <='1' when elevator > passenger and elevatorStateNext = "01" else
				'1' when elevator > desired and elevatorStateNext = "11" else
				'0';
	--determines which state the elevator should be in.  01 to go to passenger. 10 to pause. 11 to go to second floor. 00 to be stopped.				
	elevatorStateNext <= "01" when ((elevator < passenger or elevator > passenger) and elevatorState = "01") else
									"10" when (elevator = passenger and elevatorState = "01") else
									"11" when (elevatorState = "10") else
									"11" when ((elevator < desired or elevator > desired) and elevatorState = "11") else
									"00" when (elevator = desired and elevatorState = "11")
									else "00";

	register_proc : process (i_go, i_clk, i_reset)
	begin
		if(i_reset = '1') then
			elevatorState <= "00";
			elevatorChoice <= '0';
			passenger <= "0000";
			desired <= "0000";
		elsif(i_go='1') then
			passenger <= i_passengerFloor;
			desired <= i_desiredFloor;
			elevatorState <= "01";
			elevatorChoice <= elevatorChoiceNext;
		elsif(rising_edge(i_clk)) then
			elevatorState <= elevatorStateNext;
		end if;			
	end process register_proc;
end elevatorSyncFSM_arch;
