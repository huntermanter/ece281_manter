#### Lab 4

###### By: Hunter Manter

<u>Documentation:</u> I only received help from Capt Johnson.  He helped me with alot of debugging. 

Purpose: The purpose of this lab was to implement all the previous labs/homeworks into one giant project.  This would make us understand how each component worked so that we could implement them together.  A major thing that this lab emphasized was understanding the signals connecting the modules.  For the extra credit, we had to make our own FSM and add a lot of functionality requiring knowledge of how to deconflict similar modules. 



<u>Prelab:</u> 

![IMG_0790](images\IMG_0790.jpg)



<u>Questions:</u> For the 6 lights on either side, can we make tow tied together so that we don't have to complicate our code?

​		Can both elevators move at the same time? i have heard both things and I'm not sure if it would be easier to implement.

​		Is there a limit to the logic that we can put in top_basys?



Results:

- Design:

  ​	I had a lot of signals in my top_basys file because of how many times each signal needed to be used and I couldn't use a small amount of signals.  I also have three clocks to help with the organization of how time was used.  Three modules needed a clock and at different frequencies.

- Debugging section:

  ​	Unsigned integers.   Thats it.  I had one or two other minor things that took 2-3 tests to work out.  I did have to ask for help to figure out why my elevator wouldn't go up to 15 floors (because it was stopping at 12).  The problem was an easy to fix ghost state that Capt. Johnson helped me on.  The unsigned integers was my biggest problem.  I made it to full functionality and then even the first extra credit doing fine.  When implementing my second elevator, I noticed that the subtraction sign in Vivado kept throwing an error so Capt Johnson recommended a library and it stopped throwing errors.  As soon as I added the second elevator, I couldn't figure out what was wrong with my code.  The problem was that I needed to see which elevator was closest to the passenger.  To do this, I used subtraction and then comparison.  My elevators would not work and would only try going downwards, even when the elevator was at the 1st floor.  So I tried my usual debugging method of displaying what the signals and making sure they were what I wanted them to be.  Every time I displayed a signal on the display, it was what it was supposed to be.  This issue took 1.5 days of just coding.  Eventually after trying to get help and finally sending my code to Capt. Johnson it was realized that my comparisons (which were making want to go down when the MSB was on) were wrong because the inputs were passed as a signed integer.  So.  My reverted code worked just fine after I took out the library.  To implement the second elevator, I think it would take 20 minutes (if its not in my report later then thats a false estimate) because the problem was the same with the two elevators as it was for one.  My methods of trying to debug were making small changes to see the difference they made and outputting key signals to the board in the form of LEDs and to the display.

- Testing methodology: 

  ​	With this project, I did incremental coding so I tested the code after about 5 lines of code (still didn't help with my unsigned int problem but oh well).  I would test the functionality that I had just put in my code.  For example, when implementing the switches to control the elevator, I first tested that the inputs were passed properly by outputting them to the screen and then changing it so that they went into my modules and then tested the output as if there was no logic in the modules.  I then would add functionality piece by piece to test it.  To test if I had completed all the tasks required for the total functionality, I tested if the elevator could go to 15 and back.  I then tested if it could go down to one and then back up in the same test to make sure it didn't get stuck there.  I also checked that the changing of the switches during movement didn't affect the elevator.  I did these tests because either I had noticed a hiccup once during development or because it was a main issue (like the unsigned int).

- Simulation results:

  ​	The simulations that I did on key modules worked perfectly (because they were the results of other labs) and because I didn't really change them.  I did have to test the TDM just to make sure it outputted the correct thing during its cycles but it worked perfectly.

- Final results:

  ​	Link to video:

  ​	I am very happy with the results.  The board pauses at the passengers floor before moving on.  I did have to think for a bit before I implemented the thunderbird lights because I needed them to come on before I moved and not move when once the second floor is reached.  I had to think about this a lot because I was using a faster clock to do the FSM part of determining which floor should be next.  I ended up using the same clock as the elevator controller and just adjusting the logic to react quicker than the signal to move.

Observations and Conclusions: I learned that vivado is also cursed with unsigned integers.  I did not at all expect this to be a problem I encountered (although I probably should have realized it).  I learned a lot more about signals connecting modules, connecting multiple outputs to a single signal and multiple inputs to a logic vector.  I was actually having fun until I tried to put in the second elevator.  I also am not sure if setting the bit stream generation to 4 jobs makes it faster, but it feels better.

Reflections: 

- I spent ~40 hours on this lab. which I'm very disappointed in.
- The hardest part besides the signed integers (sorry I keep talking about it) was trying to think about ways to figure out how to find which elevator was the closest.  I drew it out a couple times and thought over it a bunch but eventually just ended up using simple subtraction.
- All the previous labs were helpful because I had to use their final product as a module in this one.  I also thought that the previous in class exercises were super helpful because they introduced the topics to us and made good stepping stones to launch us into this project.
- The instructions and general idea behind this lab were actually straight forward.  I knew what to do going into it.  I would recommend talking about unsigned integers just in-case someone ends up in my position.

