--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2018 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : top_basys3.vhd
--| AUTHOR(S)     : Capt Phillip Warner
--| CREATED       : 3/9/2018
--| DESCRIPTION   : This file implements the top level module for a BASYS 3 to 
--|					drive the Lab 3 Design Project (Advanced Elevator Controller).
--|
--|					Inputs: clk       --> 100 MHz clock from FPGA
--|							btnL      --> Rst Clk
--|							btnR      --> Rst FSM
--|							btnU      --> Rst Master
--|							btnC      --> GO (request floor)
--|							sw(15:12) --> Passenger location (floor select bits)
--| 						sw(3:0)   --> Desired location (floor select bits)
--| 						 - REQUIRED FUNCTIONALITY ONLY: sw(1) --> up_down, sw(0) --> stop
--|							 
--|					Outputs: led --> indicates elevator movement with sweeping pattern
--|							   - led(10) --> led(15) = MOVING UP
--|							   - led(5)  --> led(0)  = MOVING DOWN
--|							   - ALL OFF		     = NOT MOVING
--|							 an(3:0)    --> seven-segment display anode active-low enable (AN3 ... AN0)
--|							 seg(6:0)	--> seven-segment display cathodes (CG ... CA.  DP unused)
--|
--| DOCUMENTATION : None
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std
--|    Files     : MooreElevatorController.vhd, clock_divider.vhd, sevenSegDecoder.vhd
--|				   thunderbird_fsm.vhd, TDM4.vhd, OTHERS???
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;


entity top_basys3 is
	port(

		clk     :   in std_logic; -- native 100MHz FPGA clock
		
		-- Switches (16 total)
		sw  	:   in std_logic_vector(15 downto 0);
		
		-- Buttons (5 total)
		btnC	:	in	std_logic;					  -- GO
		btnU	:	in	std_logic;					  -- master_reset
		btnL	:	in	std_logic;                    -- clk_reset
		btnR	:	in	std_logic;	                  -- fsm_reset
		--btnD	:	in	std_logic;			
		
		-- LEDs (16 total)
		led 	:   out std_logic_vector(15 downto 0);

		-- 7-segment display segments (active-low cathodes CG ... CA)
		seg		:	out std_logic_vector(6 downto 0);  -- seg(6) = CG, seg(0) = CA

		-- 7-segment display active-low enables (anodes)
		an      :	out std_logic_vector(3 downto 0)
	);
end top_basys3;

architecture top_basys3_arch of top_basys3 is 
  
	-- declare components and signals
	component MooreElevatorController is
		Port ( clk : in  STD_LOGIC;
			   reset : in  STD_LOGIC; -- synchronous
			   stop : in  STD_LOGIC;
			   up_down : in  STD_LOGIC;
			   floor : out  STD_LOGIC_VECTOR (3 downto 0));
	end component MooreElevatorController;
	
	component clock_divider is
        generic ( constant k_DIV : natural := 2	); -- How many clk cycles until slow clock toggles
                                                   -- Effectively, you divide the clk double this 
                                                   -- number (e.g., k_DIV := 2 --> clock divider of 4)
        port ( 	i_clk    : in std_logic;
                i_reset  : in std_logic;		   -- asynchronous
                o_clk    : out std_logic		   -- divided (slow) clock
        );
    end component clock_divider;
	
	 component thunderbird_fsm is
    port(
		i_clk, i_reset : in std_logic;
		i_left, i_right : in std_logic;
		o_lights_L : out std_logic_vector(2 downto 0);
		o_lights_R : out std_logic_vector(2 downto 0)
);
  end component;
	
	component sevenSegDecoder is 
    port(
        i_D : in std_logic_vector(3 downto 0);
        o_S : out std_logic_vector(6 downto 0)
        );
    end component;
<<<<<<< HEAD
	
	component tdm4 is
	generic ( constant k_WIDTH : natural  := 4); -- bits in input and output
    Port ( i_CLK     	: in  STD_LOGIC;
           i_RESET	 	: in  STD_LOGIC; -- asynchronous
           i_D3 		: in  STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
		   i_D2 		: in  STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
		   i_D1 		: in  STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
		   i_D0 		: in  STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
		   o_DATA		: out STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
		   o_SEL		: out STD_LOGIC_VECTOR (3 downto 0)	-- selected data line (one-cold)
	);
	end component;
	
	component hexToDec is 
	port(
		i_D : in std_logic_vector(3 downto 0);
		o_tens : out std_logic_vector(3 downto 0);
		o_ones : out std_logic_vector(3 downto 0)
	);
	end component;
	
	component elevatorSyncFSM is
		port(
        -- Identify input and output bits here
        i_elevator1 : in std_logic_vector(3 downto 0);
        i_elevator2 : in std_logic_vector(3 downto 0);
        i_passengerFloor : in std_logic_vector(3 downto 0);
        i_desiredFloor : in std_logic_vector(3 downto 0);
        i_reset   : in  STD_LOGIC;
        i_go : in STD_LOGIC;
        i_clk : in std_logic;
        o_elevatorChoice : out std_logic;
        o_elevatorStop : out std_logic;
        o_up : out std_logic;
        o_down : out std_logic
      );
	end component;
	
	component elevatorDecoder is
		port(
		i_elevator : in std_logic;
		i_up : in std_logic;
		i_down : in  std_logic;
		i_stop : in std_logic;
		o_up1 : out std_logic;
		o_down1 : out std_logic;
		o_stop1 : out std_logic;
		o_up2 : out std_logic;
		o_down2 : out std_logic;
		o_stop2 : out std_logic
  );
    end component;
	
=======
>>>>>>> parent of 61d9da3... One elevator working and displaying up to 15 on both sections of the display
	signal clockreset : std_logic;
	signal fsmreset : std_logic;-- := btnU or btnR;
	signal clkbtwn : std_logic;
<<<<<<< HEAD
	signal clktdm : std_logic;
	signal clkthunderbird : std_logic;
	signal stop1 : std_logic;
	signal up1 : std_logic;
	signal down1 : std_logic;
	signal stop2 : std_logic;
	signal up2 : std_logic;
	signal down2 : std_logic;
	signal stop : std_logic;
	signal up	: std_logic;
	signal down : std_logic;
	signal go : std_logic;
	signal elevator : std_logic;
	signal passengerFloor : std_logic_vector(3 downto 0);
	signal desiredFloor : std_logic_vector(3 downto 0);
	signal floorbtwn1 : std_logic_vector(3 downto 0);
	signal floorbtwn2 : std_logic_vector(3 downto 0);
	signal elev1Ones : std_logic_vector(3 downto 0);
	signal elev1Tens : std_logic_vector(3 downto 0);
	signal elev2Ones : std_logic_vector(3 downto 0);
	signal elev2Tens : std_logic_vector(3 downto 0);
	signal floorToDisplay : std_logic_vector(3 downto 0);
	signal anodeSwitch : std_logic_vector(3 downto 0);
	signal ledL : std_logic_vector(2 downto 0);
	signal ledR : std_logic_vector(2 downto 0);
=======
	signal floorbtwn : std_logic_vector(3 downto 0);
>>>>>>> parent of 61d9da3... One elevator working and displaying up to 15 on both sections of the display
	constant k_clock_divs	: natural	:= 25000000;
begin
	go <= btnC;
	clockreset <= btnU or btnL;
	fsmreset <= btnU or btnR;
	-- PORT MAPS ----------------------------------------
uut_clock : clock_divider 
	generic map ( k_DIV => k_clock_divs )
	port map (
		i_clk   => clk,
		i_reset => clockreset,
		o_clk	=> clkbtwn
	);
	
	uut_controller1 : MooreElevatorController port map (
		clk     => clkbtwn,
<<<<<<< HEAD
		reset   => resetMaster,
		stop    => stop1,
		up_down => up1,
		floor   => floorbtwn1
	);
	
	uut_controller2 : MooreElevatorController port map (
		clk     => clkbtwn,
		reset   => resetMaster,
		stop    => stop2,
		up_down => up2,
		floor   => floorbtwn2
	);
	
	uut_thunder : thunderbird_fsm port map (
	  -- use comma (not a semicolon)
		i_left => up,
		i_right => down,
		i_clk => clkthunderbird,
		i_reset => resetMaster,
		o_lights_L(0) => ledL(2),
		o_lights_L(1) => ledL(1),
		o_lights_L(2) => ledL(0),
		o_lights_R(0) => ledR(2),
		o_lights_R(1) => ledR(1),
		o_lights_R(2) => ledR(0)
=======
		reset   => fsmreset,
		stop    => sw(0),
		up_down => sw(1),
		floor   => floorbtwn
	);
	
	
	uut_thunder : thunderbird_fsm port map (
	  -- use comma (not a semicolon)
		i_left => sw(15),
		i_right => sw(0),
		i_clk => clkbtwn,
		i_reset => btnR,
		o_lights_L(0) => led(15),
		--o_lights_L(0) => led(14),
		o_lights_L(1) => led(13),
		--o_lights_L(1) => led(12),
		o_lights_L(2) => led(11),
		--o_lights_L(2) => led(10),
		o_lights_R(0) => led(5),
		--o_lights_R(0) => led(4),
		o_lights_R(1) => led(3),
		--o_lights_R(1) => led(2),
		o_lights_R(2) => led(1)
		--o_lights_R(2) => led(0)
>>>>>>> parent of 61d9da3... One elevator working and displaying up to 15 on both sections of the display
	  -- no comma on last line
	);
	
	uut_seg : sevenSegDecoder port map (
	i_D => floorbtwn,
	o_S(0) => seg(6),
	o_S(1) => seg(5),
	o_S(2) => seg(4),
	o_S(3) => seg(3),
	o_S(4) => seg(2),
	o_S(5) => seg(1),
	o_S(6) => seg(0)
	);
	
<<<<<<< HEAD
	uut_tdm4 :  TDM4 port map(
		i_CLK     	=> clktdm,
		i_RESET	=> resetMaster, -- asynchronous
		i_D3 		=> elev2Tens,
		i_D2 		=> elev2Ones,
		i_D1 		=> elev1Tens,
		i_D0 		=> elev1Ones,
		o_DATA	=> floorToDisplay,
		o_SEL		=> anodeSwitch	-- selected data line (one-cold)
	);
	
	uut_HexToDec1 : hexToDec port map(
		i_D => floorbtwn1,
		o_tens => elev1Tens,
		o_ones => elev1Ones
	);
	
	uut_HexToDec2 : hexToDec port map(
		i_D => floorbtwn2,
		o_tens => elev2Tens,
		o_ones => elev2Ones
	);
	
	uut_elevatorSync : elevatorSyncFSM port map(
	-- Identify input and output bits here
        i_elevator1 => floorbtwn1,
        i_elevator2 => floorbtwn2,
        i_passengerFloor => passengerFloor,
        i_desiredFloor =>  desiredFloor,
        i_reset   => resetMaster,
        i_go => go,
        i_clk => clkthunderbird,
        o_elevatorChoice => elevator,
        o_elevatorStop => stop,
        o_up => up,
        o_down => down
  );
	
	uut_elevatorDecoder : elevatorDecoder port map(
	i_elevator => elevator,
	i_up => up,
	i_down => down,
	i_stop => stop,
	o_up1 => up1,
	o_down1 => down1,
	o_stop1 => stop1,
	o_up2 => up2,
	o_down2 => down2,
	o_stop2 => stop2
  );
=======
>>>>>>> parent of 61d9da3... One elevator working and displaying up to 15 on both sections of the display
	
	-- CONCURRENT STATEMENTS ----------------------------
	
	-- ground unused LEDs (which is all of them for REQUIRED functionality)
	--led(15 downto 0) <= (others => '0');
	led(9 downto 6) <= (others => '0');
<<<<<<< HEAD
	an	<= anodeSwitch;
	passengerFloor <= sw(15 downto 12);
	desiredFloor <= sw(3 downto 0);
=======
	an          <= (2 => '0', others => '1');
>>>>>>> parent of 61d9da3... One elevator working and displaying up to 15 on both sections of the display
	-- leave unused switches UNCONNECTED
	
	-- Ignore the warnings associated with these signals
	-- Alternatively, you can create a different board implementation, 
	--   or make additional adjustments to the constraints file
	
	-- wire up active-low 7SD anodes (an) as required
	-- Tie any unused anodes to power ('1') to keep them off
	
end top_basys3_arch;
