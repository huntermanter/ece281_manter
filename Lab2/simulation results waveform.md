![pre lab table](images\pre lab table.PNG)

![simulation capture](images\simulation capture.PNG)

In the table, the hexadecimal digits are found using the inputs on the same horizontal line which produce the outputs (also on the same line).  In the simulation, the inputs and outputs are grouped together and labeled like they are shown in the table.