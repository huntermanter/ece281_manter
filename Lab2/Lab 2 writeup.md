Title: Lab 2 (seven segment decoder)

Author: Hunter Manter

Documentation Statement:  Addison Whitney helped me by explaining the nuisances of the syntax in VHDL.    I struggled with setting up the parameters to get my code to run so he walked me through setting up the test bench and finding bugs in my code needed to generate the bit stream.

Purpose: The purpose of this lab was to understand how to implement a seven segment decoder.  It was also used to show us how to use the display and buttons on the board.  

Prelab: My work for the prelab is in the excel doc that should be in this folder.  

Results: 

- Debugging section:
  - My code had so many nots and ands that I forgot some parentheses that took a while to find
  - I had a semicolon where one shouldn't be, this took 15 minutes to find even when knowing what to look for
  - I could not generate a bitstream for a long time because I needed to uncomment 3 more lines that I thought I didn't need.  I also had my basys file in the wrong place in my project so I had to remove it and add it to the right place.
- Testing methodology:
  - First, I tried flipping random switches to see if there was any glaring problems that I needed to fix.  I was concerned with how the c and the b printed because they looked wrong but after I thought about it, it made sense. 
  - I then went in order to see if there were any more fine tune problems but everything printed out in the order that it was supposed to.
  - I would've compared my results with the truth table to see if anything was amiss but Capt. Johnson was with me and offered to check the demo and I agreed.  

Observations and Conclusions:  I thought that using the display on the board was super cool.  I was wondering during this lab if I could use the board as a calculator.  After thinking about this for a while, I realized that I would need to store the first input (using the switches as binary input) unless I figured out how to keep a signal with the same value present after a button was pressed.  I learned about the usefulness of the decoder which can make it easier to pass information over fewer lines (kinda, you need to understand on each side what is going on, so like a shared secret).  Either way, despite how much each part of the decoder took to code, it was interesting.  Once I figure out how to set up the projects without getting errors while generating the bit stream, I could explore the board more.

Reflection:

- Number of hours spent: 5
- The portion of the lab that was the hardest was editing the files after the synthesis to allow the generation of the bit stream.
- I looked at ice 1 and 2 to see how to set up the files.  Since the purposes of the lab and the ice's were different I had some problems but overall they helped.
- The instructions were vague and skipped over important parts of the code writing.  I understand that you can't tell us everything to do but when it jumps from thing to thing, its kind of confusing.