--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2018 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : stoplight_ver2.vhd
--| AUTHOR(S)     : Capt Phillip Warner
--| CREATED       : 02/22/2018
--| DESCRIPTION   : This module file implements the HW stoplight example using 
--|				  : enumerated state signals to avoid specifying encoding.
--|               : Reset is asynchronous with a default state of yellow.
--|
--| DOCUMENTATION : NONE
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std
--|    Files     : None
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity stoplight_ver2 is
    Port ( C : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           clk : in  STD_LOGIC;
           R : out  STD_LOGIC;
           Y : out  STD_LOGIC;
           G : out  STD_LOGIC);
end stoplight_ver2;

architecture Behavioral of stoplight_ver2 is

	-- create state signals

begin

	-- Next state logic ---------------------------------
	
	
	-- Alternate Next State Logic using CSAs
	--next_state <= s_green when (current_state = s_green and C = '1') else
	--			    s_green when (current_state = s_red and C = '1') else
	--			    s_yellow when (current_state = s_green and C = '0') else
	--			    s_red when (current_state = s_yellow ) else
	--			    s_red when (current_state = s_red and C = '0') else
	--			    s_yellow;
	
	
	-- state memory w/ asyncrhonous reset ---------------
	state_proc : process (  )
	begin
			-- Reset state is yellow


	end process state_proc;
	-------------------------------------------------------

	
	-- Output logic     ---------------------------------
	
	
end Behavioral;

