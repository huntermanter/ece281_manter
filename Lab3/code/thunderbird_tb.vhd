--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : thunderbird_fsm_tb.vhd (TEST BENCH)
--| AUTHOR(S)     : C9C John Doe, C8C Jane Day
--| CREATED       : 01/01/2017
--| DESCRIPTION   : This file simply provides a template for all VHDL assignments
--| 				- Be sure to include your Documentation Statement below!
--|
--| DOCUMENTATION : Include all documentation statements in main .vhd file
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : LIST ANY DEPENDENCIES
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library unisim;
  use UNISIM.Vcomponents.ALL;
  
entity thunderbird_fsm_tb is
end thunderbird_fsm_tb;

architecture test_bench of thunderbird_fsm_tb is 
	
  -- declare the component of your top-level design unit under test (UUT)
  component thunderbird_fsm is
    port(
		i_clk, i_reset : in std_logic;
		i_left, i_right : in std_logic;
		o_lights_L : out std_logic_vector(2 downto 0);
		o_lights_R : out std_logic_vector(2 downto 0)
);
  end component;

  -- declare any additional components required
  
  -- declare signals needed to stimulate the UUT inputs
	constant k_period : time := 10ns;
	signal w_L : std_logic := '0';
	signal w_R : std_logic := '0';
	signal w_clk : std_logic := '0';
	signal w_reset : std_logic := '0';
	signal w_lights_L : std_logic_vector(2 downto 0) := "000";
	signal w_lights_R : std_logic_vector(2 downto 0) := "000";
  -- also need signals for the outputs of the UUT

  
begin
	-- PORT MAPS ----------------------------------------

	-- map ports for any component instances (port mapping is like wiring hardware)
	uut_inst : thunderbird_fsm port map (
	  -- use comma (not a semicolon)
		i_left => w_L,
		i_right => w_R,
		i_clk => w_clk,
		i_reset => w_reset,
		o_lights_L => w_lights_L,
		o_lights_R => w_lights_R
	  -- no comma on last line
	);


	-- CONCURRENT STATEMENTS ----------------------------

	
	-- PROCESSES ----------------------------------------
	
	-- Provide a comment that describes each process
	-- block them off like the modules above and separate with SPACE
	-- You will at least have a test process
	clk_process : process
	begin
	   w_clk <='0';
	   wait for k_period/2;
	   
	   w_clk <= '1';
	   wait for k_period/2;
   end process clk_process;
	
	-- Test Plan Process --------------------------------
	-- Implement the test plan here.  Body of process is continuous from time = 0  
	test_process : process 
	begin
	-- Place test cases here
		w_L <= '1'; wait for 20 ns;  w_L <= '0';
		wait for 60 ns;  
		w_R <= '1'; wait for 20 ns;  w_R <= '0';
		wait for 60 ns;
		w_R <= '1'; w_L <= '1'; wait for 50 ns;  w_R <= '0';w_L <= '0';
		wait for 60 ns;
		w_L <= '1'; wait for 60 ns;  w_L <= '0';
		wait for 60 ns;  
		w_R <= '1'; wait for 60 ns;  w_R <= '0';
		wait for 60 ns;
		w_L <= '1'; wait for 20 ns;  w_reset <= '1';
		wait for 50 ns; w_reset <= '0';
		w_L <= '1'; wait for 10 ns;  w_R <= '1';
		wait for 50ns;
		w_L <= '0';  w_R <= '0';
		wait for 50ns;
		w_R <= '1'; wait for 15 ns;  w_L <= '1';
		wait for 50ns;
		w_L <= '0';  w_R <= '0';
		wait; -- wait forever
	end process;	
	-----------------------------------------------------	
	
end test_bench;
