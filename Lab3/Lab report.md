#### <u>Lab 3</u>

#### <u>By:</u> Hunter Manter

<u>Documentation:</u>  I only received help from Captain Johnson

<u>Purpose:</u> The purpose of this lab was to understand how states worked.  Knowing how to use states is important because they are used to make more complicated machines.

<u>Prelab:</u>

![diagram](./images/diagram.jpg)

![Output table](./images/Output table.PNG)

![State table](./images/State table.PNG)

![hardware schematic](./images/hardware schematic.jpg)







Equations for the states are:

s_nS1 <= (i_right and not s_cS1 and not s_cS2 and not s_cS3) or (s_cS1 and not s_cS2)
    s_nS2 <= (i_left and i_right and not s_cS1 and not s_cS2 and not s_cS3) or (not s_cS1 and s_cS2 and not s_cS3) or (not s_cS1 and not s_cS2 and s_cS3) or (s_cS1 and not s_cS2 and s_cS3)
    s_nS3 <= (i_left and i_right and not s_cS1 and not s_cS2 and not s_cS3) or (s_cS1 and not s_cS2 and not s_cS3) or (i_left and not s_cS1 and not s_cS2 and not s_cS3) or (s_cS2 and not s_cS1 and not s_cS3)

 

And the equations for my lights (outputs) are:

c_LA <= (not s_cS1 and (s_cS2 or s_cS3)) or (s_cS1 and s_cS2 and s_cS3)
	c_LB <= (not s_cS1 and s_cS2) or (s_cS2 and s_cS3)
	c_LC <= s_cS2 and s_cS3
	c_RA <= s_cS1
	c_RB <= s_cS1 and (s_cS2 or s_cS3)
	c_RC <= s_cS1 and s_cS2

<u>Results:</u>

​	<u>Design:</u> My final design does not have the equations simplified.  They are also different from my preliminary design be cause they did not work.  I finished my equations the day I turned it in so I didn't have time to take my equations and try to simplify.  Trying to fix my equations took a lot of time because I tried making equations and then testing them but they never worked because I think I didn't take other possibilities into account.  Had I figured out the equations earlier, I would have been able to test simpler equations.

​	<u>Debugging:</u> A major problem that I encountered was my equations not working when I tried implementing them in my code.  To fix this, I tried writing better equations and then implementing without a lot of success.  I finally figured out the equations and got my code to work though.  What really helped in this situation was getting my states to appear in the waveform.  My other issue was simply be being dumb.  When trying to run my code on my board, I forgot to set the top_basys as the top.  Therefore, I couldn't generate the bit-stream.  I resolved this problem by asking Capt. Johnson about the issue.

​	<u>Testing methodology:</u> At first, I had my test bed do the simple task of trying with the left input on.  This didn't work at first so I tried to make new equations by looking at my state tables.  Using the state tables was super helpful and I eventually got them right.  Throughout the testing process, when one of my simpler tests worked, I would make it more in depth to test if my equations worked exactly as they were supposed to.  I had multiple equations work at first and then fail as I added more to the test bench so I kept developing the equations.  The test bench evolved from just one switch, to the other, then to both, and then with weird scenarios like pressing reset halfway through a cycle, and then finally putting the other switch on when one was halfway through its cycle.

​	<u>Simulation results:</u> ![waveform](.\images\waveform.PNG)

​	Here is my final test bed which doesn't have all the tests I conducted because this one is more in-depth.  I was super happy when this finally worked the way it was supposed to. The wave-form was a little hard to read so I spaced out the test cases more.  The hardest part of using the waveform to check if my states were doing the right thing was knowing how the states were supposed to look even though I knew what they were supposed to do because of the state diagram.

​	<u>Final results:</u> Despite my lights looking like they start from the outside and go in, I knew I only had to fix this on the output section of the code for it to work on the board.  Everything else made sense when I got this result.

<u>Observations:</u> I thought it was really cool that we could preserve states in this even if it doesn't look how we learned it.  I learned a lot about making equations and making sure they work.  I think that I will be a lot more competent on the next assignment.

<u>Reflection:</u> I spent 15 hours at least on this lab.

​		The portion that was most difficult was creating the correct equations for the states and I figured it out by doing a lot of tests and making adjustments to my equations.

​		The previous lessons that were helpful were the last lab because I needed to reference the structure of the code again.  I didn't need to reference any other lesson/assignment though.

​		I don't have any suggestions for the future because even though I struggled to make my equations work, it helped me understand a lot better.





<u>Other Images:</u> 

![hierarchical RTL Component report](./images\hierarchical RTL Component report.PNG)

![Utilization report summary table](./images\Utilization report summary table.PNG)

These reports are what I expected because the states and the lights both depend on the input which would make them need a mux.  The states also need to store information so the registers section also makes sense.  I didn't really expect the adder though and I'm curious why one was used.





![top level diagram](./images\top level diagram.jpg)

This is also pretty much what I expected because I knew that the clock divider would be used to control the thunderbird fsm.  I didn't expect the wires to be so complicated but then again I didn't have too much time to simplify my equations.